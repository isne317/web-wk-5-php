<style>

div
{
	font-size: 1.25em;
	background-color: #f2f2f2;
	padding: 12px 12px;
	border-radius: 10px;
}

</style>

<?php

	$check = 0;
	$errorText = array();
	$pwCHECK = "";
	
	foreach($_POST as $key => $value)
	{
		switch($key)
		{
			case "forename": 
				if(preg_match('/^[A-z]{3,}$/', $value)) $check++;
				else $errorText[] = "Forename must be at least 3 characters long and doesn't contain special characters.<br>"; break;
			case "surname": 
				if(preg_match('/^[A-z]{3,}$/', $value)) $check++;
				else $errorText[] = "Surname must be at least 3 characters long and doesn't contain special characters.<br>"; break;
			case "username": 
				if(preg_match('/^[A-z0-9_-]{5,}$/', $value)) $check++;
				else $errorText[] = "Username can only contains alphanumeric, - and _ and must be at least 5 characters long.<br>"; break;
			case "password": 
				if(preg_match('/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d@$!%*?&]{8,}/', $value)) $check++;
				else $errorText[] = "Password must be at least 8 characters long and must contain at least one character from each:<br>- CAPITAL LETTERS<br>- small letters<br>- Number<br>- @$!%*?&<br>"; 
				$pwCHECK = $value; break;
			case "passwordCHECK": 
				if( $pwCHECK == $value ) $check++;
				else $errorText[] = "Confirm password doesn't match!<br>"; break;
			case "age": 
				if($value >= 18) $check++;
				else $errorText[] = "You must be at least 18 years old to register.<br>"; break;
			case "email": 
				if(preg_match('/^([A-z0-9]+)@([A-z0-9]+)(\.[A-z0-9]+)$/', $value)) $check++;
				else $errorText[] = "Email must be in the form of abc@something.com<br>"; break;
		}
	}
?>


<div>
<?php
	if($check == 7) 
	{
		echo "ALL DATA CORRECT, WELCOME!";
	}
	else 
	{
		echo implode("<br>",$errorText);
	}
?>
</div>
